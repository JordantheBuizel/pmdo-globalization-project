﻿// <copyright file="English.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Localization.En_US
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    internal class English : ILocalization
    {
        public string HoldText => "Hold";

        public string UseText => "Use";

        // Other languages can use other splash text if needed due to text not translating over with the same significance
        public List<string> SplashText => new List<string>
        {
            "Carpe diem; Seize the day!",
            "All dreams are but another reality. Never forget.",
            "Living is using the time given to you. You cannot recall lost time. Don't forget that.",
            "These donuts are great! Jelly-filled are my favorite! Nothing beats a jelly-filled donut!",
            "You said you had a dream... That dream... Make it come true! Make your wonderful dream a reality, and it will become your truth!",
            "The more wonderful the meeting, the sadder the parting.",
            "I'd rather eat Randy",
            "You finally realize it now... If you wish, and wish very strongly... Perhaps you will meet again.",
            "YOOM-TAH!",
            "Hey, I know! I'll use my trusty frying pan... As a drying pan!"
        };

        public string NewAccountBtn => "New Account";

        public string OptionsBtn => "Options";

        public string SkinsBtn => "Skins (Coming Soon!)";

        public string CreditsBtn => "Credits";

        public string LoginBtn => "Login";

        public string CopyrightText => "Pokémon Mystery Dungeon Online © 2017 JordantheBuizel, PMDO Team";

        public string LoadingText => "Loading...";

        public string GoBackText => "<-- Go Back";

        public string SelectCharText => "Select Character";

        public string UsernameText => "Username";

        public string PasswordText => "Password";

        public string ServerStatusText => "Server Status";

        public string DesiredUsernameText => "Desired Username";

        public string DesiredPasswordText => this.PasswordText;

        public string VerifyPasswordText => "Verifiy Password";

        public string PasswordEqualityText => "Passwords do not match";

        public string CreateAccountBtn => "Create Account";

        public string NewAccountTitleBarText => "Create New Account";

        public string TakeText => "Take";

        public string GiveText => "Give";

        public string RememberPasswordText => "Remember Account";

        public CultureInfo Culture => CultureInfo.CreateSpecificCulture("en-US");
    }
}
