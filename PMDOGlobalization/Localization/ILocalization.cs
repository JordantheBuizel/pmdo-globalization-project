﻿// <copyright file="ILocalization.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

using System.Globalization;

namespace Client.Logic.Localization
{
    using System.Collections.Generic;

    /// <summary>
    /// Every language class must implement this interface. The properties are also subject to change.
    /// </summary>
    public interface ILocalization
    {
        // CultureInfo
        CultureInfo Culture { get; }

        // SplashScreen Strings
        string NewAccountBtn { get; }

        string OptionsBtn { get; }

        string SkinsBtn { get; }

        string CreditsBtn { get; }

        string LoginBtn { get; }

        string CopyrightText { get; }

        string LoadingText { get; }

        string GoBackText { get; }

        string SelectCharText { get; }

        string UsernameText { get; }

        string PasswordText { get; }

        string ServerStatusText { get; }

        string RememberPasswordText { get; }

        List<string> SplashText { get; }

        // NewAccount Strings
        string DesiredUsernameText { get; }

        string DesiredPasswordText { get; }

        string VerifyPasswordText { get; }

        string PasswordEqualityText { get; }

        string CreateAccountBtn { get; }

        string NewAccountTitleBarText { get; }

        // ItemSelected Strings
        string HoldText { get; }

        string UseText { get; }

        string TakeText { get; }

        string GiveText { get; }
    }
}