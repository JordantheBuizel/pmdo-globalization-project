﻿// <copyright file="Language.cs" company="JordantheBuizel">
// Copyright (c) JordantheBuizel. All rights reserved.
// </copyright>

namespace Client.Logic.Localization
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    internal class Language
    {
        private CultureInfo currentLanguage;
        private ILocalization localizationClass;

        public Language(CultureInfo requestedLanguage)
        {
            this.currentLanguage = requestedLanguage;

#if PMDO
            Globals.Language = this;
#endif

            this.localizationClass = this.GetLanguageClass();
        }

        public Language()
        {
            this.currentLanguage = CultureInfo.InstalledUICulture;

#if PMDO
            Globals.Language = this;
#endif

            this.localizationClass = this.GetLanguageClass();
        }

        public ILocalization CurrentLanguage => this.localizationClass;

        private ILocalization GetLanguageClass()
        {
            switch (this.currentLanguage.Name)
            {
                case "en-US":
                    {
                        return new En_US.English();
                    }

                // Defaults to the code's native language
                default:
                    {
                        return new En_US.English();
                    }
            }
        }

    }
}
